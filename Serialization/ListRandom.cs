﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Serialization
{
    /// <summary>
    ///     Random Linked List
    /// </summary>
    internal class ListRandom
    {
        //Size of list
        public int Count;

        //First element in list
        public ListNode Head;

        //LAst element in list
        public ListNode Tail;

        /// <summary>
        ///     Create new node and add it to list
        /// </summary>
        /// <param name="data">Information for new node</param>
        public void AddNode(string data)
        {
            var newNode = new ListNode
            {
                Data = data
            };
            if (Count == 0)
            {
                Head = newNode;
                Tail = newNode;
            }
            else
            {
                Tail.Next = newNode;
                newNode.Previous = Tail;
                Tail = newNode;
            }

            Count++;
        }

        /// <summary>
        ///     Create random links between nodes in list
        /// </summary>
        public void SetRandomLink()
        {
            var list = new List<ListNode>();
            var node = Head;
            for (var i = 0; i < Count; i++) //Create list for get  indexes
            {
                list.Add(node);
                node = node.Next;
            }

            for (var i = 0; i < Count; i++)
                list[i].Random = list[new Random().Next(0, Count - 1)]; //Create random links between nodes
        }

        /// <summary>
        ///     Print data from each node and its random link
        /// </summary>
        public void PrintList()
        {
            var node = Head;
            for (var i = 0; i < Count; i++)
            {
                Console.WriteLine(node.Random != null
                    ? $"id: {i}, data: {node.Data}, random node data: {node.Random.Data}"
                    : $"id: {i}, data: {node.Data}, random node data: NULL");

                node = node.Next;
            }
        }

        /// <summary>
        ///     List serialization
        /// </summary>
        /// <param name="stream">Output stream for serialized list</param>
        public void Serialize(Stream stream)
        {
            var nodeIndexDictionary = new Dictionary<ListNode, int>();
            var node = Head;
            for (var i = 0; i < Count; i++) // create dictionary of ListNode and its indexes
            {
                nodeIndexDictionary.Add(node, i);
                node = node.Next;
            }

            node = Head;

            using (var binaryWriter = new BinaryWriter(stream)) // write serialized list as count of items, then id of linked random node and node data
            {
                binaryWriter.Write(Count);
                for (var i = 0; i < Count; i++)
                {
                    if (node.Random == null)
                        binaryWriter.Write(-1);
                    else
                        binaryWriter.Write(nodeIndexDictionary[node.Random]);
                    binaryWriter.Write(node.Data);
                    node = node.Next;
                }
            }
        }

        /// <summary>
        ///     List deserialization
        /// </summary>
        /// <param name="stream">Input stream for serialized list</param>
        public void Deserialize(Stream stream)
        {
            using (var binaryReader = new BinaryReader(stream))
            {
                var listNodes = new List<ListNode>();
                Count = binaryReader.ReadInt32(); //read items count
                var randomIndexes = new int[Count];
                for (var i = 0; i < Count; i++) //read and create nodes for list
                {
                    randomIndexes[i] = binaryReader.ReadInt32();
                    var node = new ListNode { Data = binaryReader.ReadString() };
                    listNodes.Add(node);
                }

                for (var i = 0; i < Count; i++) //restore random links between nodes
                {
                    if (i > 0) listNodes[i].Previous = listNodes[i - 1];

                    if (i < Count - 1) listNodes[i].Next = listNodes[i + 1];
                    if (randomIndexes[i] != -1) listNodes[i].Random = listNodes[randomIndexes[i]];
                }

                Head = listNodes[0]; //set links to head and tail 
                Tail = listNodes[Count - 1];
            }
        }
    }
}
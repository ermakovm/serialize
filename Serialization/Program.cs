﻿using System;
using System.IO;
using System.Text;

namespace Serialization
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const string fileName = "Data.bin";
            var dic = new[]
            {
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
            };

            var listForSerialization = new ListRandom();
            var listForDeserialization = new ListRandom();

            var count = new Random().Next(1, 250); //create random list
            for (var i = 0; i < count; i++)
            {
                var length = new Random().Next(1, 50);
                var dataStringBuilder = new StringBuilder();
                for (var j = 0; j < length; j++) dataStringBuilder.Append(dic[new Random().Next(0, dic.Length)]);
                listForSerialization.AddNode(dataStringBuilder.ToString());
                Console.WriteLine($"Create node with data: {listForSerialization.Tail.Data}");
            }

            listForSerialization.SetRandomLink();
            listForSerialization.PrintList();

            try
            {
                using (var serializeFileStream = new FileStream(fileName, FileMode.OpenOrCreate)) //serialize list
                {
                    listForSerialization.Serialize(serializeFileStream);
                }

                using (var deserializeFileStream = new FileStream(fileName, FileMode.Open)) //deserialized to other list
                {
                    listForDeserialization.Deserialize(deserializeFileStream);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine(exception.StackTrace);
            }

            listForDeserialization.PrintList();
            Console.WriteLine(Check(listForSerialization, listForDeserialization)); //check lists for sameness
        }

        public static bool Check(ListRandom firstList, ListRandom secondList)
        {
            if (firstList == null || secondList == null) return false;
            if (firstList.Count != secondList.Count) return false;

            var firstNode = firstList.Head;
            var secondNode = secondList.Head;
            for (var i = 0; i < firstList.Count; i++) //check every node
            {
                if (!firstNode.Data.Equals(secondNode.Data)) return false;
                if (firstNode.Random != null && secondNode.Random != null &&
                    !firstNode.Random.Data.Equals(secondNode.Random.Data)) return false;

                firstNode = firstNode.Next;
                secondNode = secondNode.Next;
            }

            return true;
        }
    }
}
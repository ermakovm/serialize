﻿namespace Serialization
{
    /// <summary>
    /// Node of Linked list
    /// </summary>
    class ListNode
    {
        //Linkl to previous node in list
        public ListNode Previous;
        //Link to next node in list
        public ListNode Next;
        //Link to random node in list
        public ListNode Random;
        //Node data
        public string Data;
    }
}
